Background
==========

Test SVG inline:
![Test SVG](./test.svg)

Test SVG inline2:
![Test SVG](test.svg)

Test SVG img:
<img src="./test.svg" alt="Test SVG"/>

Test SVG img2:
<img src="test.svg" alt="Test SVG"/>

Test SVG img3

#+html: <p align="center"><img src="test.svg" /></p>

The C++ Standard Library currently supports two types of one-dimensional
contiguous array:

- `array<n, T>` (fixed size)
- `vector<T>` (variable size)

There are no types for true multi-dimensional arrays of fixed or variable size.
Basic C-style arrays can be used as multi-dimensional arrays:

```c++
    int array3d[4][5][6];
```

However, accessing a single element involves subsetting the array multiple
times:

```c++
    int& elem = array3d[2][3][4];
```

is calling the `[]` index operator three times, returning a subset with
reduced dimensionality each time:

```c++
    int (&array2d)[5][6] = array3d[2];
    int (&array1d)[6] = array2d[3];
    int& elem = array1d[4];
```

Given the unsafe nature of C-style arrays, they are undesirable to use
generally, but they also conflate access to an element with a multi-dimensional
coordinate, here $`(2,3,4)`$, with subsetting and dimensional reduction.
They additionally do not separate the *access* to a coordinate with the
calculation of the element index, which is useful in situations where we
might want some additional abstraction, for example for sparse data.

n-dimensional array support
===========================

Classes
-------

`dimension`
  A description of a single dimensional "axis"; this may be spatial,
  temporal or an abstract dimension.  The following properties are
  required:

  name
    The name of the dimension; must be unique within a space
  extent
    The size of the dimension; must be greater than zero

  The following properties are optional:

  subrange-begin
    The starting index of the addressable range; defaults to zero
  subrange-end
    The ending index of the addressable range; defaults to the extent

  This class currently contains only the detail required for
  performing calculations.  It could be extended with additional
  metadata if required.  Examples could include axis labels, unit and
  scale, description, and whether or not the data in this dimension is
  continuously sampled.

`space`
  An abstract space containing one or more `dimension` objects.  This
  class provides the methods to perform n-dimension coordinate
  calculations within the specified abstract space.  The following
  properties are required:

  dimensions
    An ordered list of `Dimension` objects; the order
    defines the *logical* dimension order
  storage-order
    The order of the dimensions in storage (e.g. memory or disc), and
    the direction of progression; this defines the *storage* order

  From the defined logical dimension order and storage order within a
  space, the following properties are computed for each dimension:

  stride
    The number of elements to increment the element index by to move
    forward in this dimension by one element (in storage order)
  descending-offset
    The offset to add to the stride; this is used for descending
    progression (when the dimension runs backwards, the starting offset
    is not zero)

  The following properties are computed for the space

  base
    The sum of the descending-offset values for all dimensions

Calculations
------------

The following terms are used:

| Term             | Description           |
| ---------------- | --------------------- |
| $`N`$            | Number of dimensions  |
| $`E`$            | Dimension extent      |
| $`C`$            | Logical coordinate    |
| $`I_l`$          | Logical index         |
| $`I_s`$          | Storage index         |
| $`S_l`$          | Logical stride        |
| $`S_s`$          | Storage stride        |
| $`O`$            | Base offset           |
| $`B`$            | Subrange start offset |
| $`\mathrm{mod}`$ | Division remainder    |

Logical coordinate
^^^^^^^^^^^^^^^^^^

The logical coordinate describes a position in a space.  It contains
the same number of values as the number of dimensions in the space,
and the order of these values is the logical dimension order.  For
example, if the logical dimension order is `X,Y,Z,C`, a coordinate
would contain (*x*, *y*, *z*, *c*) values.

The coordinate may be implemented as e.g. a `vector` (C++)
or an array or `List` (Java).  Any of these representations
will permit the storage of an arbitrary number of integers.

Logical index
^^^^^^^^^^^^^

The logical index is the index into the multi-dimensional array
(offset from the starting position), where the storage order is the
same as the logical order, and all dimensions have ascending
progression.  This is the default if no storage order is specified.
The logical index is computed from a logical coordinate, and is the
sum of (logical-stride × dimension-index) for each dimension:

```math
  I_l = \sum\limits_{i=0}^{N-1} (S_{l_i} \cdot C_i)
```

The inverse is also provided, calculating the logical coordinate from
a logical index.  This is calculated for each dimension in *reverse*
logical order (from largest stride to smallest stride) as follows:

```math
  C_i = \frac{I_l}{S_{l_i}}

  I_l = I_l \,\mathrm{mod}\, S_{l_i}
```

This is primarily useful for testing the implementation; it is
unlikely this will be useful for most situations, where the storage
index should be preferred.

Storage index
^^^^^^^^^^^^^

The storage index is the index into the multi-dimensional array
(offset from the starting position), where the storage order is the
storage order as specified.  Here, the storage order may differ from
the logical order, and the dimensions may also have descending
progression instead of ascending progression.  The storage index is
computed from a logical coordinate, and is the pre-calculated base
offset plus the sum of (storage-stride × dimension-index) for each
dimension:

```math
  I_s = O + \sum\limits_{i=0}^{N-1} (S_{s_i} \cdot C_i)
```

The inverse is also provided, calculating the logical coordinate from
a storage index.  This is calculated for each dimension in *reverse*
storage order (from largest stride to smallest stride).  If the
dimension progression is descending, the index in each dimension is
calculated as follows:

```math
  C_i = \frac{(E_i \cdot \lvert S_{s_i} \rvert) - S_{s_i} - 1}{\lvert S_{s_i} \rvert}

  I_s = I_s \,\mathrm{mod}\, \lvert S_{s_i} \rvert
```

If the dimension progression is ascending, the index in each
dimension is calculated as follows:

```math
  C_i = \frac{I_s}{S_{s_i}}

  I_s = I_s \,\mathrm{mod}\, S_{s_i}
```

Subranges and subvolumes
------------------------

It is possible to restrict the total hypervolume within a space to a
subvolume, by specifying subrange-begin and subrange-end values to set
a subrange in each single dimension.  This effectively creates a
window into the full range, logically indexed from zero at the
subrange-begin start position in each dimension.

The calculations described above work the same with subvolumes.  The
only difference is that the subrange-begin values for each dimension
must be added to the logical coordinate when computing a storage
index, and be subtracted from the logical coordinate when computed
from a storage index.  Since the base for logical indexes is always
zero, and the coordinates in the subrange are relative to the subrange
start, no adjustment is necessary for logical index calculations.  The
storage index calculations are adjusted as shown below:

The storage index is computed from a logical coordinate, and is the
pre-calculated base offset plus the sum of (storage-stride ×
(dimension-index + dimension-start)) for each dimension:

```math
  I_s = O + \sum\limits_{i=0}^{N-1} (S_{s_i} \cdot (C_i + B_i))
```

The inverse is also provided, calculating the logical coordinate from
a storage index.  This is calculated for each dimension in *reverse*
storage order (from largest stride to smallest stride), subtracting
the subrange-begin value from the computed index in each dimension.
If the dimension progression is descending, the index in each
dimension is calculated as follows:

```math
  C_i = \frac{(E_i \cdot \lvert S_{s_i} \rvert) - S_{s_i} - 1}{\lvert S_{s_i} \rvert} - B_i

  I_s = I_s \,\mathrm{mod}\, \lvert S_{s_i} \rvert
```

If the dimension progression is ascending, the index in each
dimension is calculated as follows:

```math
  C_i = \frac{I_s}{S_{s_i}} - B_i

  I_s = I_s \,\mathrm{mod}\, S_{s_i}
```

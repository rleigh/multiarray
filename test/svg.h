/*
 * Copyright © 2015-2017 Roger Leigh <rleigh@codelibre.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 */

#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iosfwd>
#include <sstream>

#include <codelibre/multiarray/storage.h>
#include <codelibre/multiarray/util.h>

namespace test
{

  inline void
  svg_header(std::ostream& os,
             int width,
             int height,
             int boxwidth,
             int boxheight)
  {
    os << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
      "<!-- Generator: codelibre multiarray -->\n"
"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n"
      "<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"" << width << "in\" height=\"" << height << "in\" viewBox=\"0 0 " << boxwidth << " " << boxheight << "\">\n";
  }

  inline void
  svg_footer(std::ostream& os)
  {
    os << "</svg>\n";
  }

  // Print a 2D array.
  // planecoord is the index into the higher dimension (>=2) indices;
  // the lowest two dimensions will be rendered in a 2D grid.
  template<class Space>
  inline void
  dump_svg(std::ostream& os,
           const Space& space,
           typename Space::coord_type planecoord,
           uint32_t index_size,
           int translatex,
           int translatey)
  {
    if (space.size() == 0)
      return;



    std::string begin_svg = "<svg>\n";
    std::string end_svg = "<svg>\n";

    uint32_t nx = 1;
    uint32_t ny = 1;

    const auto logical = space.get_logical_order();
    nx = logical.at(0).size();
    if (space.size() > 1)
      {
        ny = logical.at(1).size();
      }

    os <<
      "  <svg font-family=\"Helvetica\">\n"
      "    <g id=\"group\" transform=\"translate(" << translatex << "," << translatey << ")\">\n"
      "      <g id=\"framebg\" style=\"fill:cornflowerblue;fill-opacity:0.5;stroke-width:0.5;stroke:cornflowerblue\">\n"
      "        <rect x=\"0\" y=\"-8\" width=\"" << 8*nx << "\" height=\"8\"/>\n"
      "        <rect x=\"-8\" y=\"0\" width=\"8\" height=\"" << 8*ny << "\"/>\n"
      "      </g>\n"
      "      <g id=\"lines\" style=\"stroke-width:0.25;stroke:cornflowerblue\">\n";
    for (uint32_t i = 1; i < nx; ++i)
      os <<
        "        <line x1=\"" << 8*i << "\" y1=\"-8\" x2=\"" << 8*i << "\" y2=\"0\"/>\n";
    for (uint32_t i = 1; i < ny; ++i)
      os <<
        "        <line x1=\"-8\" y1=\"" << 8*i << "\" x2=\"0\" y2=\"" << 8*i << "\"/>\n";
    os <<
      "      </g>\n"
      "      <g id=\"lines\" style=\"stroke-width:0.25;stroke:cornflowerblue\">\n";
    for (uint32_t i = 1; i < nx; ++i)
      os <<
        "        <line x1=\"" << 8*i << "\" y1=\"0\" x2=\"" << 8*i << "\" y2=\"" << 8*ny << "\"/>\n";
    for (uint32_t i = 1; i < ny; ++i)
      os <<
        "        <line x1=\"0\" y1=\"" << 8*i << "\" x2=\"" << 8*nx << "\" y2=\"" << 8*i << "\"/>\n";
    os <<
      "      </g>\n"
      "      <g id=\"frame\" style=\"fill-opacity:0;stroke-width:0.5;stroke:cornflowerblue\">\n"
      "        <rect x=\"0\" y=\"-8\" width=\"" << 8*nx << "\" height=\"8\"/>\n"
      "        <rect x=\"-8\" y=\"0\" width=\"8\" height=\"" << 8*ny << "\"/>\n"
      "        <rect x=\"0\" y=\"0\" width=\"" << 8*nx << "\" height=\"" << 8*ny << "\"/>\n"
      "      </g>\n"
      "      <g id=\"axis-names\" font-size=\"6\" font-style=\"italic\" fill=\"black\">\n"
      "        <g id=\"axis0\">\n"
      "          <text x=\"" << (8*nx)/2 << "\" y=\"-12\" text-anchor=\"middle\"\n"
      "                alignment-baseline=\"middle\">x→</text>\n"
      "        </g>\n"
      "        <g id=\"axis1\" transform=\"translate(-12, " << (8*ny)/2 << ")\">\n"
      "          <text text-anchor=\"middle\" alignment-baseline=\"middle\"\n"
      "                transform=\"rotate(270)\">←y</text>\n"
      "        </g>\n"
      "      </g>\n"
      "      <g id=\"xlab\" style=\"font-size:6;fill:white;text-anchor:middle\">\n";
    for (uint32_t i = 0; i < nx; ++i)
      os <<
        "        <text x=\"" << (8*i) + 4 << "\" y=\"-2\">" << i << "</text>\n";
    os <<
      "      </g>\n"
      "      <g id=\"ylab\" style=\"font-size:6;fill:white;text-anchor:middle\">\n";
    for (uint32_t i = 0; i < ny; ++i)
      os <<
        "        <text x=\"-4\" y=\"" << (8*i) + 6 << "\">" << i << "</text>\n";
    os <<
      "      </g>\n"
      "      <g id=\"labels\" style=\"font-size:6;fill:black;text-anchor:middle\">\n";

    typename Space::coord_type index;
    index.push_back(0);
    if (space.size() > 1)
      index.push_back(0);
    std::copy(planecoord.begin(), planecoord.end(),
              std::back_inserter(index));

    for (uint32_t y = 0; y < ny; ++y)
      {
        if (space.size() > 1)
          index.at(1) = y;

        for (uint32_t x = 0; x < nx; ++x)
          {
            index.at(0) = x;
            os <<
              "        <text x=\"" << (8*x) + 4 << "\" y=\"" << (8*y) + 6 << "\" >" << space.index(index) << "</text>\n";
          }
      }
    os <<
      "      </g>\n"
      "      <g id=\"fill\" style=\"fill:black;fill-opacity:0.5\">\n"
      "        <rect x=\"8\" y=\"8\" width=\"8\" height=\"8\" fill-opacity=\"0.4\">\n"
      "          <animate id=\"animlab0\" attributeName=\"opacity\"\n"
      "                   values=\"1;0;0;0;0;0;1;1;1;1;1;1\" begin=\"1s\"\n"
      "                   dur=\"4s\" repeatCount=\"indefinite\"/>\n"
      "        </rect>\n"
      "      </g>\n"
      "    </g>\n"
      "  </svg>\n";

  }

  // Print an nD array.
  template<class Space>
  inline void
  dump_svg(std::ostream& os,
           const Space& space)
  {
    const Space logical_space(make_logical_space(space, true, true));

    auto labels = std::vector<std::string>();
    for (decltype(space.size()) i = 0;
         i < space.size();
         ++i) {
      std::ostringstream l;
      l << 'D' << i;
      labels.push_back(l.str());
    }

    // Maximum length of index for display.
    uint32_t index_size = 1;
    {
      typename Space::coord_type idx;
      for(typename Space::size_type i = 0; i < space.num_elements(); ++i)
        {
          logical_space.coord(i, idx);
          index_size = std::max(index_size,
                                static_cast<uint32_t>(std::floor(std::log10(static_cast<float>(space.index(idx))))) + 1);
        }
    }

    svg_header(os, 6, 6, 128, 128);

    if (space.size() < 3) // Render as 1D or 2D plane
      {
        typename Space::coord_type planecoord; // Intentionally empty.
        dump_svg(os, space, planecoord, index_size, 18, 18);
      }
    else // Render as multiple 2D planes
      {
        const auto logical = logical_space.get_logical_order();

        std::vector<typename Space::dimension_type> dims;
        for (uint32_t i = 2; i < logical.size(); ++i)
          dims.push_back(logical.at(i));
        Space space2(dims);

        typename Space::coord_type coord;
        for(typename Space::size_type i = 0; i < space2.num_elements(); ++i)
          {
            space2.coord(i, coord);
            std::ostringstream lab;
            for (uint32_t j = 0; j < coord.size(); ++j)
              {
                const typename Space::dimension_type& dim(logical.at(j + 2));
                lab << labels.at(j+2) << '=' << coord.at(j);
                if (j + 1 < coord.size())
                  lab << ", ";
              }
            os <<
              "<text font-family=\"Helvetica\" style=\"font-size:6;fill:black\" x=\"" << 6 << "\" y=\"" << (6 + (i * ((logical.at(1).size() * 8) + 36))) << "\" >" << lab.str() << "</text>\n";
            dump_svg(os, space, coord, index_size, 18, 18 + (i * ((logical.at(1).size() * 8) + 36)));
            std::cout << '\n';
          }
      }

    svg_footer(os);
  }

}


/*
 * Local Variables:
 * mode:C++
 * End:
 */

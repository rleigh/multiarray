include(CMakeFindDependencyMacro)
include(${CMAKE_CURRENT_LIST_DIR}/CodelibreMultiArrayInternal.cmake)

add_library(Codelibre::MultiArray INTERFACE IMPORTED)
set_target_properties(Codelibre::MultiArray PROPERTIES INTERFACE_LINK_LIBRARIES multiarray)

# multiarray

Multi-dimensional array arithmetic


Overview
--------

To allow the storage, transfer, transformation and subsetting of
n-dimensional pixel data.

Details
-------

This library supports several different multi-dimensional array implementations, with differing tradeoffs:

